module bitbucket.com/scraper

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1
	go.mongodb.org/mongo-driver v1.5.3
)
