package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Client struct {
	db *mongo.Client
}

func GetConnection(connectURI string) (*Client, error) {
	client, err := mongo.Connect(context.Background(), options.Client().
		ApplyURI(connectURI))
	if err != nil {
		return nil, err
	}

	return &Client{
		db: client,
	}, nil
}

func (c *Client) InsertValue(database, collection string) {
	_, err := c.db.
		Database(database).
		Collection(collection).
		InsertOne(context.Background(), bson.D{{"name", "pi"}, {"value", 3.14159}})
	if err != nil {
		panic(err)
	}
}
