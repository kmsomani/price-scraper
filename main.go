package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	url := "https://economictimes.indiatimes.com/commoditysummary/symbol-COTTON.cms"
	selector := ".commodityPrice"

	price, err := getPrice(url, selector)
	if err != nil {
		log.Fatal("cannot get commodity price:", err.Error())
	}
	fmt.Println(price)

	// connectURI := "Connect URI here"
	// db, err := mm.GetConnection(connectURI)
	// if err != nil {
	// 	log.Fatal("cannot get mongo client:", err.Error())
	// }

}

func getPrice(url, selector string) (float64, error) {
	resp, err := http.Get(url)
	if err != nil {
		return 0, fmt.Errorf("cannot reach URL %s (%v)", url, err)
	}
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return 0, fmt.Errorf("cannot read response (%v)", err)
	}

	items := doc.Find(".commodityPrice")
	if items.Size() != 1 {
		if err != nil {
			return 0, fmt.Errorf("unexpected number items for selector (%s - %d matches)", selector, items.Size())
		}
	}

	price := ""
	doc.Find(".commodityPrice").Each(func(i int, s *goquery.Selection) {
		price = s.Text()
	})

	return strconv.ParseFloat(price, 64)
}
